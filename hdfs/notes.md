
testando o namenode a partir do openjdk11 (vem com apt)
teste: docker run -it --rm openjdk:11 /bin/bash

features:
hdfs namenode start
criação do datanode

https://files.grouplens.org/datasets/movielens/ml-20m-youtube.zip

docker cp /home/wsl/dsa/datalake-projeto1/hdfs/ml-20m-youtube.zip hdfs-hdp_namenode-1:/home/hduser/dados 

hdfs dfs -put ml-20m-youtube.zip /user/hduser/arquivos

hdfs dfs -mkdir /user

hdfs dfs -mkdir /user/hduser

hdfs dfs -ls /

hdfs dfs -mkdir /user/hduser/arquivos

hdfs dfs -ls /user/hduser/arquivos

docker exec -u hduser -it hdfs-hdp_namenode-1 bash