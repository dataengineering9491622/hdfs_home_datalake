#!/usr/bin/env sh

set -eux

# Shortcircuit for non-default commands.
# The last part inside the "{}" is a workaround for the following bug in ash/dash:
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=874264
if [ -n "${1:-}" ] && [ "${1#-}" = "${1}" ] \
  && [ -n "$(command -v -- "${1}")" ] \
  && { ! [ -f "${1}" ] || [ -x "${1}" ]; }; then
  exec "$@"
fi

for arg in "$@"; do
    if [ "$arg" = "datanode" ]; then
        mkdir -p /home/hduser/hdfs/datanode
        chown hduser:hduser /home/hduser/hdfs/datanode
        chmod 755 /home/hduser/hdfs/datanode
        break
    elif [ "$arg" = "namenode" ]; then
        mkdir -p /home/hduser/hdfs/namenode
        chown hduser:hduser /home/hduser/hdfs/namenode
        chmod 755 /home/hduser/hdfs/namenode
        break
    fi
done

# { echo "jun"; } </dev/null &>/dev/null & disown

# habilitar a sustentação do JPS, Storage directory /home/hduser/hdfs/namenode has been successfully formatted.
echo "Y" | su hduser -s /home/hduser/hadoop/bin/hdfs -- namenode -format

exec su hduser -s /home/hduser/hadoop/bin/hdfs -- "$@"
