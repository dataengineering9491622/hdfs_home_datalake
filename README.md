

# Project: Hadoop Home

![img](https://images.unsplash.com/opengraph/1x1.png?blend=https%3A%2F%2Fimages.unsplash.com%2Fphoto-1516132006923-6cf348e5dee2%3Fblend%3D000000%26blend-alpha%3D10%26blend-mode%3Dnormal%26crop%3Dfaces%252Cedges%26h%3D630%26mark%3Dhttps%253A%252F%252Fimages.unsplash.com%252Fopengraph%252Fsearch-input.png%253Fh%253D84%2526txt%253Ddata%252Blake%2526txt-align%253Dmiddle%25252Cleft%2526txt-clip%253Dellipsis%2526txt-color%253D000000%2526txt-pad%253D80%2526txt-size%253D40%2526txt-width%253D660%2526w%253D750%2526auto%253Dformat%2526fit%253Dcrop%2526q%253D60%26mark-align%3Dmiddle%252Ccenter%26mark-w%3D750%26w%3D1200%26auto%3Dformat%26fit%3Dcrop%26q%3D60%26ixid%3DM3wxMjA3fDB8MXxzZWFyY2h8M3x8ZGF0YSUyMGxha2V8ZW58MHx8fHwxNzE1MTAyNDQ1fDA%26ixlib%3Drb-4.0.3&blend-w=1&h=630&mark=https%3A%2F%2Fimages.unsplash.com%2Fopengraph%2Flogo.png&mark-align=top%2Cleft&mark-pad=50&mark-w=64&w=1200&auto=format&fit=crop&q=60)




# Project Overview: Local HDFS Cluster with Docker Compose

### What?
This project sets up an HDFS (Hadoop Distributed File System) cluster using Docker Compose for local management, with persistent volumes for data storage. This allows for a reliable, lightweight and consistent environment to experiment with HDFS locally.

### Why?
At a specific project, we use a hybrid approach for our data lake, with HDFS as the foundational component for our on-premises solution. Having a local environment for experimentation is crucial for understanding the underlying infrastructure and testing some data processing approaches. This setup provides a practical way to gain hands-on experience and develop a deeper understanding of HDFS and its application in the architecture of data lakes.

# Installation and Setup

### Prerequisites
Ensure you have the following installed on your local machine:

* Docker Engine
* Docker Compose

I like to structure it as below - 
## Clone the Repository
```
git clone https://gitlab.com/dataengineering9491622/hdfs_home_datalake
cd hdfs_home_datalake
```

## Build and Start the Containers
Use Docker Compose to build the images and start the cluster:
```
docker compose up --build

```

## Verify the Setup

Check the status of the containers to ensure they are running:
```
docker compose ps

```
Access the NameNode web UI at http://localhost:9870

![img](images/HadoopHomePage.PNG)

## Source Data and Testing

Dataset: https://files.grouplens.org/datasets/movielens/ml-20m-youtube.zip

```


hdfs dfs -mkdir /user

hdfs dfs -mkdir /user/hduser

hdfs dfs -mkdir /user/hduser/data

hdfs dfs -ls /



#access the namenode

docker exec -u hduser -it hdfs-hdp_namenode-1 bash

docker cp ml-20m-youtube.zip hdfs-hdp_namenode-1:/home/hduser/data 

hdfs dfs -put ml-20m-youtube.zip /user/hduser/data

```

Repository tree  .
```bash
.
├── data-processing
│   └── compose.yaml
├── hdfs
│   ├── compose.yaml
│   ├── config-files
│   │   ├── core-site.xml
│   │   ├── hadoop-env.sh
│   │   ├── hdfs-site-datanode.xml
│   │   ├── hdfs-site-namenode.xml
│   │   ├── hdfs-site.xml
│   │   └── workers
│   ├── docker
│   │   ├── Dockerfile
│   │   ├── entrypoint.sh
│   │   └── ssh_config
│   ├── ml-20m-youtube.zip
│   └── notes.md
├── images
│   ├── HadoopDataNodePage.PNG
│   └── HadoopHomePage.PNG
├── README_GITLAB.md
└── README.md
```

# Results and evaluation
* Setting up the hadoop enviroment can be challenging but it can be also automated
* This project automated HDFS setup for local experimentation, extracting the most out of Docker Compose, reducing a lot the setup time compared to manual installation. Also it solves the problem "only works on my machine" since the enviroments are isolated.
* Data persistency is achieved by adding volumes to the datanodes. So the experiments can be run on several days without worring about shutting down the machine. (Dont forget to execute docker compose down :)
* Code Reuse in Docker-Compose Using YAML Anchor Feature
* Configuration reuse was applied and for the services only 1 docker image was build.
* All steps are automated through docker and docker compose and bash scripts

# Future work
For future work there are some ideas:
* Auto load sample data
* Include associated data processing services

# Contact
https://www.linkedin.com/in/victoremanuelms/

# Acknowledgments
Especial acknowledgement to my friend Vitor Vasconcellos and to Data Science Academy for providing challenges, collaboraiton and learning. 

For instance, I am referencing the image that I used for my readme header - 
- Image by [unspash](https://www.vectorstock.com/royalty-free-vector/data-science-cartoon-template-with-flat-elements-vector-27984292)

# License

For this github repo, the License used is [MIT License](https://opensource.org/license/mit/).